# Практическая работа №2
## Описание программы
Предположим, что есть некий холдинг по изготовлению мебели. У него есть много заводов(в коде реализовано 2), которые изготавливают разные части мебели с разными характеристиками. Для данной модели хорошо подойдет паттерн "Абстрактная фабрика", который и будет реализовываться. Поскольку холдин большой, то у него есть главный(командный) отдел, который решает что делает определенный завод, собирет мебель из камплектующих или же ликвидирует их из-за брака. Для данной модели хорошо подойдет паттерн "Команда".
## Абстрактная фабрика (Abstract Factory)
Паттерн "Абстрактная фабрика" (Abstract Factory) предоставляет интерфейс для создания семейств взаимосвязанных объектов с определенными интерфейсами без указания конкретных типов данных объектов.
## Команда (Command)
Паттерн "Команда" (Command) позволяет инкапсулировать запрос на выполнение определенного действия в виде отдельного объекта. Этот объект запроса на действие и называется командой. При этом объекты, инициирующие запросы на выполнение действия, отделяются от объектов, которые выполняют это действие.




## UML программы
```plantuml
@startuml

skin rose

title Classes - Class Diagram

Interface ITabletop{
    void Dimensions();
    void Material();
}
Interface ILegs{
    void Dimensions();
    void Material();
}
Interface IBackrest{
    void Dimensions();
    void Material();
}
class WoodenTabletop {
  +Dimensions()
  +Material()
}
class GlassTabletop {
  +Dimensions()
  +Material()
}
class MetalLegs {
  +Dimensions()
  +Material()
}
class GlassBackrest {
  +Dimensions()
  +Material()
}
class MetalBackrest {
  +Dimensions()
  +Material()
}
Interface IFurnitureFactory{
  ITabletop CreateTabletop();
  ILegs CreateLegs();
   IBackrest CreateBackrest();
}
Interface IFurnitureCommand{
  void Execute();
}
class FurnitureCommandCreate {
  -ITabletop _tabletop;
  -ILegs _legs;
  -IBackrest _backrest;
  Execute()
}
class FurnitureCommandDel {
  -ITabletop _tabletop;
  -ILegs _legs;
  -IBackrest _backrest;
  Execute()
}
class WoodenFurnitureFactory {
  +ITabletop CreateTabletop()
  +ILegs CreateLegs()
  +IBackrest CreateBackrest()
}
class MetalFurnitureFactory {
  +ITabletop CreateTabletop()
  +ILegs CreateLegs()
  +IBackrest CreateBackrest()
}
MetalFurnitureFactory --->  IFurnitureFactory
WoodenFurnitureFactory --->  IFurnitureFactory

WoodenTabletop <-- WoodenFurnitureFactory
MetalLegs <-- WoodenFurnitureFactory
GlassBackrest <-- WoodenFurnitureFactory
GlassTabletop <-- MetalFurnitureFactory
MetalLegs <-- MetalFurnitureFactory
MetalBackrest <-- MetalFurnitureFactory

ITabletop <--- WoodenTabletop
ILegs <--- GlassTabletop
IBackrest <--- MetalLegs
ITabletop <--- GlassBackrest
IBackrest<--- MetalBackrest

IFurnitureCommand <- FurnitureCommandCreate
IFurnitureCommand <- FurnitureCommandDel
FurnitureCommandDel -> IFurnitureFactory
FurnitureCommandCreate -> IFurnitureFactory


@enduml
```
