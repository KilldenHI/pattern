﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace pattern
{
    //Интерфейсы частей мебели
    public interface ITabletop
    {
        void Dimensions();
        void Material();
    }
    public interface ILegs
    {
        void Dimensions();
        void Material();
    }
    public interface IBackrest
    {
        void Dimensions();
        void Material();
    }
    //Конкретная реализация частей мебели
    public class WoodenTabletop : ITabletop
    {
        public void Dimensions()
        {
            Console.WriteLine("Деревянная столешница: 100 x 60 cm");
        }

        public void Material()
        {
            Console.WriteLine("Деревянная столешница: Березовая древесина");
        }
    }
    public class GlassTabletop : ITabletop
    {
        public void Dimensions()
        {
            Console.WriteLine("Стеклянная столешница: 80 x 80 cm");
        }

        public void Material()
        {
            Console.WriteLine("Стеклянная столешница: Витраное стекло");
        }
    }
    public class MetalLegs : ILegs
    {
        public void Dimensions()
        {
            Console.WriteLine("Металлические ножки: 80cm");
        }

        public void Material()
        {
            Console.WriteLine("Металлические ножки: Сплав титана");
        }
    }
    public class GlassBackrest : IBackrest
    {
        public void Dimensions()
        {
            Console.WriteLine("Стеклянная спинка: 40 x 60 cm");
        }

        public void Material()
        {
            Console.WriteLine("Стеклянная спинка: Каленое стекло");
        }
    }
    public class MetalBackrest : IBackrest
    {
        public void Dimensions()
        {
            Console.WriteLine("Металлическая спинка: 30 x 80 cm");
        }

        public void Material()
        {
            Console.WriteLine("Металлическая спинка: Стальные болты Т-800");
        }
    }
    //Интерфейс Абстрактной фабрики
    public interface IFurnitureFactory
    {
        ITabletop CreateTabletop();
        ILegs CreateLegs();
        IBackrest CreateBackrest();
    }
    //Конкретная фабрика ("Деревянная" мебельная фабрика)
    public class WoodenFurnitureFactory : IFurnitureFactory
    {
        public ITabletop CreateTabletop()
        {
            return new WoodenTabletop();
        }

        public ILegs CreateLegs()
        {
            return new MetalLegs();
        }

        public IBackrest CreateBackrest()
        {
            return new GlassBackrest();
        }
    }
    //Конкретная фабрика ("Металлическая" мебельная фабрика)
    public class MetalFurnitureFactory : IFurnitureFactory
    {
        public ITabletop CreateTabletop()
        {
            return new GlassTabletop();
        }

        public ILegs CreateLegs()
        {
            return new MetalLegs();
        }

        public IBackrest CreateBackrest()
        {
            return new MetalBackrest();
        }
    }
    //Интерфейс, представляющий команду
    public interface IFurnitureCommand
    {
        void Execute();
    }
    //Конкретная реализация команды, реализует метод Execute(), в котором происходит создание мебели
    public class FurnitureCommandCreate : IFurnitureCommand
    {
        private readonly ITabletop _tabletop;
        private readonly ILegs _legs;
        private readonly IBackrest _backrest;

        public FurnitureCommandCreate(IFurnitureFactory factory)
        {
            _tabletop = factory.CreateTabletop();
            _legs = factory.CreateLegs();
            _backrest = factory.CreateBackrest();
        }

        public void Execute()
        {
            Console.WriteLine("Создание мебели...");
            _tabletop.Dimensions();
            _tabletop.Material();
            _legs.Dimensions();
            _legs.Material();
            _backrest.Dimensions();
            _backrest.Material();
            Console.WriteLine("Мебель создана!");
        }
    }
    //Конкретная реализация команды, реализует метод Execute(), в котором происходит удаление мебели
    public class FurnitureCommandDel : IFurnitureCommand
    {
        private readonly ITabletop _tabletop;
        private readonly ILegs _legs;
        private readonly IBackrest _backrest;

        public FurnitureCommandDel(IFurnitureFactory factory)
        {
            _tabletop = factory.CreateTabletop();
            _legs = factory.CreateLegs();
            _backrest = factory.CreateBackrest();
        }

        public void Execute()
        {
            Console.WriteLine("Ликвидация мебели...");
            _tabletop.Dimensions();
            _tabletop.Material();
            _legs.Dimensions();
            _legs.Material();
            _backrest.Dimensions();
            _backrest.Material();
            Console.WriteLine("Мебель ликвидирована!");
        }
    }
    //Основная программа
    class Program
    {
        static void Main(string[] args)
        {
            //Создание "деревянной" фабрики 
            IFurnitureFactory woodenFactory = new WoodenFurnitureFactory();
            IFurnitureCommand woodFurnitureCommand = new FurnitureCommandCreate(woodenFactory);
            //Создание "металлической" фабрики
            IFurnitureFactory metalFactory = new MetalFurnitureFactory();
            IFurnitureCommand metalFurnitureCommand = new FurnitureCommandDel(metalFactory);
            //Команда создания мебели для "деревянной" фабрики
            woodFurnitureCommand.Execute();
            //Команда удаления мебели для "металлической" фабрики
            metalFurnitureCommand.Execute();

            Console.ReadLine();
        }
    }
}
